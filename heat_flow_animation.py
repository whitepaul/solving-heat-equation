import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.animation import FuncAnimation
plt.style.use('dark_background')

tmax = 10
tcount = 100
tstep = tmax/tcount
points = 1000
heat_cmap = cm.get_cmap('viridis')
X = np.linspace(0, 2*np.pi, points)
T = np.linspace(0, tmax, 100)

a = 0
b = 2*np.pi
K = 0.25
omega = np.pi/(b - a) 
approx_to_member = 10

#Fourier coefficients
C = [-0.0452707393683613,
0.000000000000000,
-0.0452707393683613,
0.000000000000000,
-0.623627532115182,
1.57079632679490,
-0.628760269005019,
0.000000000000000,
-0.0654742098302747,
0.000000000000000,
-0.0216978099931199]

YT = [sum([C[n] * np.sin(omega * n * X) * np.exp(- K**2 * (omega * n)**2 * t) for n in range(approx_to_member)]) for t in T]

fig = plt.figure(constrained_layout=False, figsize=(10, 10))
spec = fig.add_gridspec(ncols=1, nrows=2, width_ratios=[1], height_ratios=[2, 0.1], hspace=0.2)
ax0 = fig.add_subplot(spec[0, 0])
ax0.set(xlabel='x', ylabel='temparature',
       title='Temparature in the rod. Initial: x*sin(x)*cos(x)')

vmin = YT[0].min()
vmax = YT[0].max()
print(vmin)
ax0.axis([a, b, vmin, vmax])

ax1 = fig.add_subplot(spec[1, 0])
ax1.set_axis_off()
ax1.set(xlabel = 'Rod')


#plot initial function
l = ax0.scatter(X, YT[0], s=2)
l.set_array(YT[0])
l.set_cmap(heat_cmap)

#plot initial distribution of the heat on the rod
extent = [a, b, 0, 0.2]

ax1.imshow([YT[0], YT[0]], extent=extent, vmin=vmin, vmax=vmax, cmap=heat_cmap)


def animate(t):
    l.set_offsets(np.column_stack((X, YT[t])))
    l.set_array(YT[t])
    ax1.imshow([YT[t], YT[t]], extent=extent, vmin=vmin, vmax=vmax, cmap=heat_cmap)
    fig.canvas.draw_idle()
    return l,



#def init():
 #   line.set_data([], [])
 #   return line, 

anim = FuncAnimation(fig, animate, frames=50, interval=100, blit=True)
anim.save('sine_wave.mp4', writer='ffmpeg', dpi=1000)
