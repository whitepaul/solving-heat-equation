
# About

The project aims to create model of heat flow in a rod. It is described by __heat equation__.
There are multiple special cases of HE, where solution can be found simlier than in gerenal case.
Some of them was implemented:

- homogenous, 0 boundary conditions.

- non-homogenous, 0 boundary conditions, 0 initial condition

- non-homogenous, constant temparature on the bounds

# Project structure

- __script.ipynb.__ _Sagemath_ script, contains all mathematical computations. THere are3 code fragments that compute solution in special cases mentioned above.
- __heat_flow_animation.py__ and __heat_flow_interactive.py__ build animated and interactive 2d plots of temparature evolution. They require Fourier coefficients you can obtain by _script.ipynb_

# What to improve

- __Replace symbolic integration by numerical__.
All integrals was found symbolically in _script.ipynb_.
But that would work only for simple input functions.

- __Write results of mathematical computations  in file.__

- __Add boundary conditions to model: derivative dU(x, t)/dx = 0 on the bounds.__ That is naturally how heat flows in isolated rod.

# References

- [Heat equation on Wikipedia](https://en.wikipedia.org/wiki/Heat_equation)
- Тихонов А. Н., Самарский А. А. Уравнения математической физики. — гл. III. — Любое издание.
